export const SIGN_UP_PHOTO_URL = 'https://iamt.spbstu.ru/userfiles/images/albums/B32I0065.jpg';
export const NAVBAR_LOGO_URL = "https://www.spbstu.ru/upload/branding/logo_main_sm.svg";

export const API_BASE = process.env.REACT_APP_BASE_API || 'http://127.0.0.1:8000/api/v1/'
export const USER_TOKEN_KEY = 'authToken';
