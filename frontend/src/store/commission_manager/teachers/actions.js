export const TEACHERS_LOAD = 'TEACHERS_LOAD';
export const TEACHERS_SET = 'TEACHERS_SET';

export function loadTeachers() {
  return {type: TEACHERS_LOAD};
}

export function setTeachers(teachers) {
  return {type: TEACHERS_SET, payload: teachers};
}