export const COMMISSIONS_LOAD = 'COMMISSIONS_LOAD';
export const COMMISSIONS_ADD = 'COMMISSIONS_ADD';
export const COMMISSIONS_SET = 'COMMISSIONS_SET';
export const COMMISSIONS_DELETE = 'COMMISSIONS_DELETE';

export function loadCommissions() {
  return {type: COMMISSIONS_LOAD};
}

export function setCommissions(commissions) {
  return {type: COMMISSIONS_SET, payload: commissions};
}

export function addCommission(commission) {
  return {type: COMMISSIONS_ADD, payload: commission};
}

export function deleteCommission(id) {
  return {type: COMMISSIONS_DELETE, payload: id};
}