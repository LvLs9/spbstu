export const EDITOR_OPEN = 'EDITOR_OPEN';
export const EDITOR_CLOSE = 'EDITOR_CLOSE';
export const EDITOR_SET_DATE = 'EDITOR_SET_DATE;';
export const EDITOR_ADD_TEACHER = 'EDITOR_ADD_TEACHER';
export const EDITOR_REMOVE_TEACHER = 'EDITOR_REMOVE_TEACHER';
export const EDITOR_SET_NEXT_STAGE = 'EDITOR_SET_NEXT_STAGE';
export const EDITOR_SET_PREV_STAGE = 'EDITOR_SET_PREV_STAGE';
export const EDITOR_SET_FIELD_CURRENT_ID = 'EDITOR_SET_FIELD_CURRENT_ID';
export const EDITOR_EDIT_CURRENT = 'EDITOR_EDIT_CURRENT';

export function openEditor(commission) {
  return {type: EDITOR_OPEN, payload: commission};
}

export function closeEditor() {
  return {type: EDITOR_CLOSE};
}

export function setDate(date) {
  return {type: EDITOR_SET_DATE, payload: date};
}

export function addTeacher(id) {
  return {type: EDITOR_ADD_TEACHER, payload: id};
}

export function removeTeacher(id) {
  return {type: EDITOR_REMOVE_TEACHER, payload: id};
}

export function setNextStage() {
  return {type: EDITOR_SET_NEXT_STAGE};
}

export function setPrevStage() {
  return {type: EDITOR_SET_PREV_STAGE};
}

export function editCurrent(id, callback) {
  return {type: EDITOR_EDIT_CURRENT, payload: id, callback};
}