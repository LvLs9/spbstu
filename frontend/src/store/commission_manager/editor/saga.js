import {selectCommission, selectEditor, selectFilterParams} from './selectors';
import {all, call, put, select} from '@redux-saga/core/effects';
import {CONTROL_TYPE, COURSE, DEPARTMENT, EDUCATION_FORM, INSTITUTE, SUBJECT,} from '../filter/filter_fields';
import {setFieldCurrentId} from '../filter/actions';
import axios from 'axios';
import {API_BASE} from '../../../settings';
import {EDITOR_FILTER} from '../settings';
import {startLoading, stopLoading} from '../../backdrop/actios';
import {loadCommissions} from '../commissions/actions';
import {showMessage} from '../../snackbar/actions';

export function* setEditorFilters() {
  const commission = yield select(selectCommission);
  yield all([
    EDUCATION_FORM,
    CONTROL_TYPE,
  ].map(field => {
    const action = setFieldCurrentId(field, EDITOR_FILTER)(commission[field]);
    return put(action);
  }));
  yield put(setFieldCurrentId(COURSE, EDITOR_FILTER, true)(commission.year));
  const institute = yield call(axios.get, 'institutes/',
      {params: {department: commission[DEPARTMENT]}});
  yield put(
      setFieldCurrentId(INSTITUTE, EDITOR_FILTER, true)(
          institute.data[0].id));
  yield put(
      setFieldCurrentId(DEPARTMENT, EDITOR_FILTER, true)(
          commission.department));
  yield put(setFieldCurrentId(SUBJECT, EDITOR_FILTER)(commission.subject));
}

export function* editCurrent({payload, callback}) {
  try {
    yield put(startLoading());
    const [editor, filter_params] = yield all([
      select(selectEditor),
      select(selectFilterParams),
    ]);
    const teachers = editor.editableTeacherIDs.map(id => ({
      id: id,
      is_main: id === payload,
    }));
    const response = yield call(axios.patch,
        API_BASE + `commissions/${editor.commission.id}/`,
        Object.assign({}, filter_params, {
          date: editor.commission.date,
          teachers: teachers,
        }),
    );
    yield put(loadCommissions());
    callback();
    yield put(showMessage('Успешно отредактировано', 'success'));
  } catch (e) {
    yield put(showMessage('Что-то пошло не так', 'warning'));
  } finally {
    yield put(stopLoading());
  }
}