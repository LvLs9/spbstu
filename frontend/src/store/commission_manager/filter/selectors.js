import {createSelector} from 'reselect';
import {selectCommissionManager} from '../selectors';
import {
  CONTROL_TYPE,
  COURSE,
  DEPARTMENT,
  EDUCATION_FORM,
  SUBJECT,
} from './filter_fields';

export const selectFilter = createSelector(
    selectCommissionManager,
    commissionManager => commissionManager.filter,
);

export const selectField = (fieldName) => createSelector(
    selectFilter, filter => (filter[fieldName]),
);

export const selectFieldCurrentId = (fieldName) => createSelector(
    selectField(fieldName), field => field.currentID,
);

export const selectFilterParams = createSelector(selectFilter,
    filter => Object.entries({
      department: DEPARTMENT,
      year: COURSE,
      education_form: EDUCATION_FORM,
      control_type: CONTROL_TYPE,
      subject: SUBJECT,
    }).reduce((obj, [param, field_name]) => {
      obj[param] = filter[field_name].currentID;
      return obj;
    }, {}));