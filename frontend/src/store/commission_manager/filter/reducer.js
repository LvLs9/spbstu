import {COMMISSION_FILTER_SET_FIELD_CURRENT_ID, COMMISSION_FILTER_SET_FIELD_ID_TO_VALUE,} from './actions';
import {CONTROL_TYPE, COURSE, DEPARTMENT, EDUCATION_FORM, INSTITUTE, SUBJECT,} from './filter_fields';

const makeState = () => ({idToValue: {}, currentID: ''});

const initialState = [
  INSTITUTE,
  DEPARTMENT,
  COURSE,
  EDUCATION_FORM,
  CONTROL_TYPE,
  SUBJECT,
].reduce((obj, field) => {
  obj[field] = makeState();
  return obj;
}, {});

export function createNamedFilterReducer(filterName = '') {
  return function filterReducer(state = initialState, action) {
    const {name, field} = action;
    if (name !== filterName) {
      return state;
    }
    let oldFieldState;
    switch (action.type) {
      case COMMISSION_FILTER_SET_FIELD_CURRENT_ID:
        oldFieldState = state[field];
        return {
          ...state,
          [field]: {...oldFieldState, currentID: action.payload},
        };
      case COMMISSION_FILTER_SET_FIELD_ID_TO_VALUE:
        oldFieldState = state[field];
        return {
          ...state,
          [field]: {...oldFieldState, idToValue: action.payload},
        };
      default:
        return state;
    }
  };
}