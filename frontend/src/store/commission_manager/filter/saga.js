import axios from 'axios';
import {call} from 'redux-saga/effects';
import {all, put, select, takeEvery} from '@redux-saga/core/effects';
import {
    COMMISSION_FILTER_LOAD_DATA,
    COMMISSION_FILTER_SET_FIELD_CURRENT_ID,
    COMMISSION_FILTER_SET_FIELD_ID_TO_VALUE,
    setFieldCurrentId,
    setFieldIdToValue,
} from './actions';
import {CONTROL_TYPE, COURSE, DEPARTMENT, EDUCATION_FORM, INSTITUTE, SUBJECT,} from './filter_fields';
import {startLoading, stopLoading} from '../../backdrop/actios';
import Swal from 'sweetalert2';

export function* filterSaga() {
  yield takeEvery(COMMISSION_FILTER_LOAD_DATA, initCommissionMaker);
  yield takeEvery(COMMISSION_FILTER_SET_FIELD_CURRENT_ID,
      checkCurrentIdSetting);
  yield takeEvery(COMMISSION_FILTER_SET_FIELD_ID_TO_VALUE, clearFieldId);
}

export function* initCommissionMaker({name}) {
  yield put(startLoading());
  try {
    const [institutes, years, education_forms, control_type] = yield all([
      call(axios.get, 'institutes'),
      call(axios.get, 'years'),
      call(axios.get, 'education_forms'),
      call(axios.get, 'control_types'),
    ]);
    yield put(setFieldIdToValue(INSTITUTE, institutes.data.reduce(
        (obj, institute) => {
          obj[institute.id.toString()] = institute.name;
          return obj;
        }, {},
    ), name));

    yield put(setFieldIdToValue(COURSE, years.data.reduce(
        (obj, year) => {
          obj[year.id.toString()] = year.course.toString();
          return obj;
        }, {},
    ), name));

    yield put(setFieldIdToValue(CONTROL_TYPE, control_type.data.reduce(
        (obj, control_type) => {
          obj[control_type.id.toString()] = control_type.name;
          return obj;
        }, {},
    ), name));

    yield put(setFieldIdToValue(EDUCATION_FORM, education_forms.data.reduce(
        (obj, form) => {
          obj[form.id.toString()] = form.name;
          return obj;
        }, {},
    ), name));
  } catch (e) {
    Swal.fire('Хм...', `Что-то пошло не так: ${e.message}`, 'warning');
  } finally {
    yield put(stopLoading());
  }
}

export function* clearFieldId({field, name, isInitial}) {
  if (!isInitial) {
    yield put(setFieldCurrentId(field, name)(''));
  }
}

export function* checkCurrentIdSetting(action) {
  if (action.field === INSTITUTE) {
    const departments = yield call(axios.get,
        'departments', {params: {institute_id: action.payload}},
    );
    yield put(setFieldIdToValue(DEPARTMENT, departments.data.reduce(
        (obj, department) => {
          obj[department.id.toString()] = `${department.name} ${department.ext_num}`;
          return obj;
        }, {},
    ), action.name, action.isInitial));
  }
  const fieldSelector = action.fieldSelector;
  if ([COURSE, DEPARTMENT].includes(action.field)) {
    const subjects = yield call(axios.get,
        'subjects', {
          params: {
            year_id: yield select(fieldSelector(COURSE)),
            department_id: yield select(fieldSelector(DEPARTMENT)),
          },
        },
    );
    yield put(setFieldIdToValue(SUBJECT, subjects.data.reduce(
        (obj, subject) => {
          obj[subject.id.toString()] = subject.name;
          return obj;
        }, {},
    ), action.name, action.isInitial));
  }
}