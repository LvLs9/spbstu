import {all, call, put, select, takeLatest} from '@redux-saga/core/effects';
import {selectCreator} from './selectors';
import {COMMISSION_CREATOR_CREATE} from './actions';
import {selectFilterParams} from '../filter/selectors';
import {startLoading, stopLoading} from '../../backdrop/actios';
import axios from 'axios';
import {showMessage} from '../../snackbar/actions';
import {addCommission} from '../commissions/actions';

export function* creatorSaga() {
  yield takeLatest(COMMISSION_CREATOR_CREATE, createCommission);
}

function* createCommission({payload, close}) {
  try {
    yield put(startLoading());
    const [creator, filter_params] = yield all([
      select(selectCreator),
      select(selectFilterParams),
    ]);
    const teachers = creator.teacher_ids.map(id => ({
      id: id,
      is_main: id === payload,
    }));
    const response = yield call(axios.post, 'commissions/',
        Object.assign({}, filter_params, {
          date: creator.date,
          teachers: teachers,
        }),
    );
    debugger
    yield put(addCommission(response.data));
    close();
    yield put(showMessage('Комиссия создана', 'success'));
  } catch (e) {
  } finally {
    yield put(stopLoading());
  }
}