import {
  COMMISSION_CREATOR_ADD_TEACHER,
  COMMISSION_CREATOR_REMOVE_TEACHER,
  COMMISSION_CREATOR_SET_DATE,
} from './actions';

const initialState = {
  teacher_ids: [],
  date: new Date(),
};

export default function creatorReducer(state = initialState, action) {
  switch (action.type) {
    case COMMISSION_CREATOR_SET_DATE:
      return {...state, date: action.payload};
    case COMMISSION_CREATOR_ADD_TEACHER:
      return {
        ...state,
        teacher_ids: state.teacher_ids.concat([action.payload]),
      };
    case COMMISSION_CREATOR_REMOVE_TEACHER:
      return {
        ...state,
        teacher_ids: state.teacher_ids.filter(id => id !== action.payload),
      };
    default:
      return state;
  }
}