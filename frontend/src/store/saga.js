import {takeLatest} from 'redux-saga/effects';
import {AUTH_TRY_LOGIN} from './auth/actions';
import {login_user} from './auth/saga';
import {commissionManagerSaga} from './commission_manager/saga';

export function* mySaga() {
  yield takeLatest(AUTH_TRY_LOGIN, login_user);
  yield commissionManagerSaga();
}