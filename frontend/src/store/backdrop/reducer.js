import {BACKDROP_ADD_LOADING, BACKDROP_REMOVE_LOADING} from './actios';

const initialState = 0;

export default function backdropReducer(state = initialState, action) {
  switch (action.type) {
    case BACKDROP_ADD_LOADING:
      return state + 1;
    case BACKDROP_REMOVE_LOADING:
      return Math.max(0, state - 1);
    default:
      return state;
  }
}