export const BACKDROP_ADD_LOADING = 'BACKDROP_ADD_LOADING';
export const BACKDROP_REMOVE_LOADING = 'BACKDROP_REMOVE_LOADING';

export function startLoading() {
  return {type: BACKDROP_ADD_LOADING};
}

export function stopLoading() {
  return {type: BACKDROP_REMOVE_LOADING};
}