import {AUTH_SET_EMAIL, AUTH_SET_PASSWORD} from './actions';

const initialState = {
  email: '',
  password: '',
};

export default function auth(state = initialState, action) {
  switch (action.type) {
    case AUTH_SET_EMAIL:
      return {...state, email: action.payload};
    case AUTH_SET_PASSWORD:
      return {...state, password: action.payload};
    default:
      return state;
  }
}