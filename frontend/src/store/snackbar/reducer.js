import {SNACKBAR_HIDE, SNACKBAR_SHOW_MESSAGE} from './actions';

const initialState = {
  message: null,
  severity: null,
  open: false,
  autoHideDuration: null,
};

export default function snackbarReducer(state = initialState, action) {
  switch (action.type) {
    case SNACKBAR_SHOW_MESSAGE:
      return {...state, ...action.payload, open: true};
    case SNACKBAR_HIDE:
      return {...state, open: false};
    default:
      return state;
  }
}