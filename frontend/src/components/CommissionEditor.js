import React from 'react';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import {connect} from 'react-redux';
import {
  selectEditor,
  selectField,
} from '../store/commission_manager/editor/selectors';
import {
  addTeacher,
  closeEditor as close,
  editCurrent,
  removeTeacher,
  setDate,
  setNextStage,
  setPrevStage,
} from '../store/commission_manager/editor/actions';
import DatePicker from './CommissionCreator/DatePicker';
import TeacherPicker from './CommissionCreator/TeacherPicker';
import {selectTeachers} from '../store/commission_manager/teachers/selectors';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import {labelsWithFields} from './CommisionManager';
import {bindActionCreators} from 'redux';
import {makeTextFieldGetter} from './MainFilterWithCreatorAndExcel';
import Grid from '@material-ui/core/Grid';
import {setFieldCurrentId} from '../store/commission_manager/filter/actions';
import {EDITOR_FILTER} from '../store/commission_manager/settings';
import Confirmation from './CommissionCreator/ConfirmationWindow';

function CommissionEditor({isOpen, editCurrent, commission, stage, editableTeacherIDs, close, setDate, teachers, addTeacher, removeTeacher, setNextStage, setPrevStage, fields, setters}) {
  function generateForm() {
    const final_fields = [];
    for (let i in fields) {
      const field = fields[i];
      final_fields.push([
        field[0],
        field[1],
        setters[i],
      ]);
    }
    const getTextField = makeTextFieldGetter();
    const textFields = final_fields.map(getTextField);
    return <Grid container spacing={2}>
      {textFields}
    </Grid>;
  }

  const stage_to_content = {
    0: generateForm(),
    1: <><DatePicker date={commission.date} setDate={setDate}/>
      <Box m={2}/>
      <TeacherPicker teachers={teachers}
                     editableTeacherIDs={editableTeacherIDs}
                     addTeacher={addTeacher} removeTeacher={removeTeacher}/></>,
  };
  return <Dialog open={isOpen} onClose={close}>
    <DialogTitle>Редактирование</DialogTitle>
    <DialogContent>
      {stage_to_content[stage]}
    </DialogContent>
    <DialogActions>
      {stage > 0 && <Button onClick={setPrevStage}>Назад</Button>}
      {stage === 1 ? <Confirmation teachers={teachers}
                                   creatableTeachers={editableTeacherIDs}
                                   label={'Изменить'}
                                   action={editCurrent}
                                   callback={close}
          /> :
          <Button onClick={setNextStage}>Далее</Button>}
    </DialogActions>
  </Dialog>;
}

const mapStateToProps = state => ({
  ...selectEditor(state),
  teachers: selectTeachers(state),
  fields: labelsWithFields.map(
      ([label, field]) => [label, selectField(field)(state)]),
});
const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({
    close,
    addTeacher,
    removeTeacher,
    setDate,
    setNextStage,
    setPrevStage,
    editCurrent,
  }, dispatch),
  setters: bindActionCreators(
      labelsWithFields.map(obj => setFieldCurrentId(obj[1], EDITOR_FILTER)),
      dispatch,
  ),
});
export default connect(mapStateToProps, mapDispatchToProps)(CommissionEditor);