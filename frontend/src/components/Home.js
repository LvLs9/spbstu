import React from 'react';
import {Box, Grid} from '@material-ui/core';
import {makeStyles} from '@material-ui/styles';
import {MAKE_COMMISSION} from '../urls';
import {Link as RouterLink} from 'react-router-dom';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme) => ({
  paper: {
    minHeight: '20vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    // TODO: maybe have way to solve both hardcoded colors
    background: theme.palette.primary.main,
    '&:hover': {
      background: theme.palette.primary.dark,
    },
  },
}));

const url_to_caption = {
  [MAKE_COMMISSION]: 'Создать коммиссию',
};

export default function Home() {
  const classes = useStyles();
  return <Grid container spacing={1}>
    {
      Object.entries(url_to_caption).map(
          ([url, caption]) => <Grid key={url} item xs={12}>
            <Link component={RouterLink} to={url} underline='none'>
              <Button className={classes.paper} component={Box} elevation={2}
                     color='white'>
                <Typography variant="h4" gutterBottom
                            align='center'>{caption}</Typography>
              </Button>
            </Link>
          </Grid>,
      )}
  </Grid>;
}