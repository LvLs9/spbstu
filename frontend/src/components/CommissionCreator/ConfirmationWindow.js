import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import React, {useState} from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

function Confirmation({teachers, creatableTeachers, action, callback, label='Создать'}) {
  const [isOpen, setOpen] = useState(false);
  let close = () => setOpen(false);
  return <>
    <Button onClick={() => setOpen(true)} variant='contained' color="primary">
      {label}
    </Button>
    <Dialog open={isOpen} onClose={close}>
      <DialogTitle>Подтверждение</DialogTitle>
      <DialogContent>
        Выберите председателя комиссии для подтверждения
      </DialogContent>
      <DialogActions>
        <List style={{width: '100%'}}>{
          creatableTeachers.map(id =>
              <ListItem button key={id} onClick={() => {
                close();
                action(id, callback);
              }}>
                <ListItemText>{teachers[id]}</ListItemText>
              </ListItem>,
          )}
        </List>
      </DialogActions>
    </Dialog>
  </>;
}

export default Confirmation;