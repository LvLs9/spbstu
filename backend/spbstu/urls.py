from django.urls import include, path
from rest_framework import routers

from spbstu.views import (
    CommissionViewSet,
    ControlTypeViewSet,
    DepartmentViewSet,
    EducationFormViewSet,
    DownloadExcelAPIView,
    InstituteViewSet,
    SubjectViewSet,
    TeacherViewSet,
    YearViewSet, UploadExcelAPIView
)

router = routers.SimpleRouter()
router.register(r'years', YearViewSet)
router.register(r'institutes', InstituteViewSet)
router.register(r'departments', DepartmentViewSet)
router.register(r'education_forms', EducationFormViewSet)
router.register(r'control_types', ControlTypeViewSet)
router.register(r'teachers', TeacherViewSet)
router.register(r'subjects', SubjectViewSet)
router.register(r'commissions', CommissionViewSet)
urlpatterns = router.urls

urlpatterns += [
    path('auth/', include('djoser.urls')),
    path('auth/', include('djoser.urls.authtoken')),
    path('download/', DownloadExcelAPIView.as_view()),
    path('upload/', UploadExcelAPIView.as_view()),
]
