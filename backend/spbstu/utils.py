import datetime


def get_year(course_number):
    now = datetime.datetime.now()

    if now.month >= 9:
        course_number -= 1

    return datetime.datetime(now.year - course_number, 9, 1)
