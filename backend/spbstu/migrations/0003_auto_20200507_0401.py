# Generated by Django 3.0.6 on 2020-05-07 01:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('spbstu', '0002_auto_20200503_2224'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClassRoom',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
            ],
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64)),
            ],
        ),
        migrations.RemoveField(
            model_name='teacher',
            name='birth_date',
        ),
        migrations.AddField(
            model_name='scheduleitem',
            name='is_on_even_week',
            field=models.BooleanField(default=None),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='scheduleitem',
            name='time',
            field=models.TimeField(default=None),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='student',
            name='document',
            field=models.CharField(default=None, max_length=128),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='student',
            name='document_number',
            field=models.CharField(default=None, max_length=128),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='student',
            name='is_contract',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='student',
            name='nationality',
            field=models.CharField(default=None, max_length=128),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='teacher',
            name='financing_source',
            field=models.CharField(default=None, max_length=128),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='teacher',
            name='rate',
            field=models.FloatField(default=None),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='scheduleitem',
            name='class_room',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.PROTECT, to='spbstu.ClassRoom'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='student',
            name='status',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.PROTECT, to='spbstu.Status'),
            preserve_default=False,
        ),
    ]
